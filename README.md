# LinkedList #

This code implements a templated singly linked list. It will also keep track of the count of elements in the list.

# Features #
* Templated for any type
* Peek into Front/Back elements
* Pop Front/Back element
* Push data onto the Front/Back
* Keeps count of elements in the list