#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <string> // Used for the string data type
#include <iostream> // Used for cout, endl
#include <iomanip> // Used for setw, left

// LinkedList
// This class is a list container where each element is connected by pointers. It is
// dynamic such that it can grow indefinitely.
template <class T>
class LinkedList {
	private:
		// ListNode
		// This is the node struct that the LinkedList class uses for each
		// element in the list
		struct ListNode {
			ListNode * next;
			T data;
		};
		
		ListNode * head, * tail; // Pointers to the beginning and end of the list
		int count; // The amount of elements in the list
		
	public:
		// Default Constructor
		// Initializes the list to nothing
		LinkedList() {
			head = NULL;
			tail = NULL;
			count = 0;
			
			return;
		}
		
		// Parameterized Constructor
		// params:
		//	list: This it the list we wish to copy
		// Initializes the list to be the same as the passed list
		LinkedList(const LinkedList & list) {
			ListNode * curNode = list.head, * prevNode = NULL, * newHead = NULL, * newTail = NULL;
			int newCount = 0;
			
			// While there are nodes to copy
			while (curNode != NULL) {
				// Create a new node
				ListNode * newNode = new ListNode;
				newNode->next = NULL;
				newNode->data = curNode->data;
				
				// If there is no head node
				if (newHead == NULL) {
					// Make this node the head node
					newHead = newNode;
					newTail = newNode;
				} else {
					// Add this node to the tail
					newTail->next = newNode;
					newTail = newNode;
				}
				
				// Move to the next node
				prevNode = curNode;
				curNode = curNode->next;
				newCount++;
			}
			
			// Assign the new list
			head = newHead;
			tail = newTail;
			count = newCount;
			
			return;
		}
		
		// Destructor
		// Makes sure all node are safely removed from memory
		~LinkedList() {
			Clear();
			return;
		}
		
		// PushFront
		// params:
		//	data: the data to be held in a node
		// Adds a new node to the front of the list with the given data
		void PushFront(T data) {
			ListNode * newNode = new ListNode;
			
			newNode->next = head;
			newNode->data = data;
			
			if (head == NULL) {
				head = newNode;
				tail = newNode;
			} else {
				head = newNode;
			}
			
			count++;
		}
		
		// PushBack
		// params:
		//	data: the data to be held in a node
		// Adds a new node to the end of the list with the given data
		void PushBack(T data) {
			// Create a new node
			ListNode * newNode = new ListNode;
			
			newNode->next = NULL;
			newNode->data = data;
			
			// If there is no head node
			if (head == NULL) {
				// Make this node the head
				head = newNode;
				tail = newNode;
			} else {
				// Add this node to the tail
				tail->next = newNode;
				tail = newNode;
			}
			
			count++;
			
			return;
		}
		
		// Front
		// Returns the data at the front of the list
		T * Front() {
			if (Empty())
				return NULL;
			return &(head->data);
		}
		
		// End
		// Returns the data at the back of the list
		T * End() {
			if (Empty())
				return NULL;
			return &(tail->data);
		}
		
		// PopFront
		// Removes the node at the front of the list and returns its data
		T * PopFront() {
			// If there is nothing in the front of the list
			if (Empty())
				return NULL;
			
			// Take the first node out and move the head to the next node
			ListNode * poppedNode = head;
			head = head->next;
			
			// Delete the node so it is no longer in memory
			T * poppedData = &(poppedNode->data);
			delete poppedNode;
			count--;
			
			return poppedData;
		}
		
		// PopBack
		// Removes the node at the back of the list and returns its data
		T * PopBack() {
			if (Empty())
				return NULL;
			
			ListNode * current = head;
			
			// Get to the node before the last node
			while (current->next->next != NULL) {
				current = current->next;
			}
			
			// Delete the node so it is no longer in memory
			T * data = &(current->next->data);
			delete(current->next);
			count--;
			
			current->next = NULL;
			
			return data;
		}
		
		// At
		// Returns the data at the given index, or NULL if the index is invalid
		T * At(int index) {
			if (Empty() || index < 0 || index >= count)
				return NULL;
			
			int i = 0;
			ListNode * current = head;
			T * data;
			
			while (i < index) {
				current = current->next;
				i++;
			}
			
			return &(current->data);
		}
		
		// Clear
		// Deletes every node in the list from memory
		void Clear() {
			ListNode * current = head, * next;
			
			// While there is still a node to remove
			while (current != NULL) {
				// Get the next node
				next = current->next;
				
				// Remove the current node from memory and the list
				delete current;
				
				// Go to the next node
				current = next;
			}
			
			// Remove any dangling pointers
			head = NULL;
			tail = NULL;
			count = 0;
			
			return;
		}
		
		// Empty
		// Returns true if the list has no nodes in it
		bool Empty() const {
			return (head == NULL);
		}
		
		// Count
		// Returns the count of elements currently in the list
		int Count() const {
			return count;
		}
		
		// Print
		// Prints the data for every node in the list. format: Item #i\t: {data}
		void Print() const {
			// If the list is not empty
			if (!Empty()) {
				int id = 0;
				
				std::cout << "-- List Count : " << count << std::endl;
				
				// For every node
				for (ListNode * node = head; node != NULL; node = node->next) {
					// Print its information
					std::cout << " #" << std::left << std::setw(5) << id << ": " << node->data << std::endl;
					++id;
				}
			} else { // If the list is empty
				std:: cout << "The list is empty, there is nothing to print!" << std::endl;
			}
			
			return;
		}
		
		// operator= Overload
		// params:
		//	list: This it the list we wish to copy
		// Deletes the current list and sets this equal to a copy of the given list
		LinkedList & operator=(const LinkedList & list) {
			if (this != &list) {
				ListNode * curNode = list.head, * prevNode = NULL, * newHead = NULL, * newTail = NULL;
				int newCount = 0;
				
				// Clear the list of any current contents
				Clear();
				
				// While there are nodes to copy
				while (curNode != NULL) {
					// Create a new node and copy its data
					ListNode * newNode = new ListNode;
					newNode->next = NULL;
					newNode->data = curNode->data;
					
					// If we dont have a head node
					if (newHead == NULL) {
						// Make this node the head
						newHead = newNode;
						newTail = newNode;
					} else {
						// Add this node to the tail
						newTail->next = newNode;
						newTail = newNode;
					}
					
					// Move to the next node
					prevNode = curNode;
					curNode = curNode->next;
					newCount++;
				}
				
				// Assign the new list to this list
				head = newHead;
				tail = newTail;
				count = newCount;
			}
			
			return *this;
		}
};

#endif
