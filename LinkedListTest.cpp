#include <iostream>
#include <cstdlib>
#include <assert.h>

#include "LinkedList.h"

using namespace std;

int main() {
	// Constructor and Peek tests
	LinkedList<int> list;
	
	assert(list.Empty());
	assert(list.Front() == NULL);
	assert(list.End() == NULL);
	
	// Push test
	list.PushFront(2);
	
	assert(!list.Empty());
	assert(*(list.Front()) == 2);
	assert(*(list.End()) == 2);
	assert(list.Count() == 1);
	
	list.PushFront(1);
	
	assert(!list.Empty());
	assert(*(list.Front()) == 1);
	assert(*(list.End()) == 2);
	assert(list.Count() == 2);
	
	list.PushBack(3);
	
	assert(!list.Empty());
	assert(*(list.Front()) == 1);
	assert(*(list.End()) == 3);
	assert(list.Count() == 3);
	
	// Clear tests
	assert(!list.Empty());
	list.Clear();
	assert(list.Empty());
	list.Clear();
	assert(list.Empty());
	
	// At tests
	list.PushFront(2);
	list.PushFront(1);
	list.PushBack(3);
	
	assert(list.At(-1) == NULL);
	assert(list.At(3) == NULL);
	assert(*(list.At(0)) == 1);
	
	list.Clear();
	
	assert(list.At(0) == NULL);
	
	// Pop Tests
	list.PushBack(1);
	list.PushBack(2);
	list.PushBack(3);
	list.PushBack(4);
	list.PushBack(5);
	
	int * data = list.PopFront();
	
	assert(list.Count() == 4);
	assert(*data == 1);
	
	data = list.PopBack();
	
	assert(list.Count() == 3);
	assert(*data == 5);
	
	// Print test
	list.Print();
	
	cout << "All tests successful." << endl;
	
	return 0;
}